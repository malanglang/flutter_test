import 'package:flutter/material.dart';

import '../app_widgets/first_app_bar.dart';

class SettingHome extends StatelessWidget with FirstAppBar {
  const SettingHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getNormalAppBar(context, '设置'),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.arrow_back),
        elevation: 0.0,
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
