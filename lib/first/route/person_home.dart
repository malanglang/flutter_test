
import 'package:flutter/material.dart';

import '../app_widgets/first_app_bar.dart';

class PersonHome extends StatelessWidget with FirstAppBar {
  const PersonHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getNormalAppBar(context, '个人中心'),
    );
  }
}
