
import 'package:flutter/material.dart';
import '../app_widgets/first_app_bar.dart';

class MessageHome extends StatelessWidget with FirstAppBar {
  const MessageHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getNormalAppBar(context, '消息'),
    );
  }
}