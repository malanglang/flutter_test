

import 'package:flutter/material.dart';

import '../app_widgets/first_app_bar.dart';

class HomePage extends StatelessWidget with FirstAppBar {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: getAppbar(context, '首页'),
    );
  }
}