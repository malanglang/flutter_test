

import 'package:flutter/material.dart';

class Utils {
  static void pushRoute(BuildContext context, Widget page) {
    // 添加新的路由 跳转页面
    Navigator.of(context).push(
      MaterialPageRoute(
          builder: (BuildContext context) {
            return page;
          }
      ),
    );
  }
}