import 'package:flutter/material.dart';
import 'package:flutter_app_test/first/route/message_home.dart';
import 'package:flutter_app_test/first/route/setting_home.dart';
import 'package:flutter_app_test/first/utils.dart';

import '../route/person_home.dart';

mixin FirstDrawer {

  Drawer getDrawer(BuildContext context) {

    var drawer = Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            currentAccountPicture: const CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://img0.baidu.com/it/u=3493060908,1285666718&fm=253&fmt=auto&app=120&f=JPEG?w=640&h=438'),
            ),
            decoration: BoxDecoration(
                color: Colors.yellow[400],
                image: const DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.linearToSrgbGamma(),
                  image: NetworkImage(
                      'https://img0.baidu.com/it/u=1528181606,2562079957&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889'),
                )),
            accountName: const Text("name",
                style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            accountEmail: const Text("email@qq.com",
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal,
                    color: Colors.black54)),
          ),
          ListTile(
            trailing: const Icon(Icons.arrow_forward_ios, size: 16,),
            leading: const Icon(Icons.message),
            title: const Text('Messages'),
            onTap: () {
              Navigator.pop(context); // close the drawer
              // 添加新的路由 跳转页面
              Navigator.pushNamed(context, 'message');
            },
          ),
          ListTile(
            trailing: const Icon(Icons.arrow_forward_ios, size: 16,),
            leading: const Icon(Icons.account_circle),
            title: const Text('Profile'),
            onTap: () {
              Navigator.pop(context); // close the drawer
              Navigator.pushNamed(context, 'person');
            },
          ),
          ListTile(
            trailing: const Icon(Icons.arrow_forward_ios, size: 16,),
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () {
              Navigator.pop(context); // close the drawer
              // 添加新的路由 跳转页面
              Navigator.pushNamed(context, 'setting');
            },
          ),
        ],
      ),
    );
    return drawer;
  }

}
