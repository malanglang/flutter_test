import 'package:flutter/material.dart';

mixin FirstAppBar {

  PreferredSizeWidget getAppbar(BuildContext context, String title) {
    bool showSearch = true;
    if (title == '首页') showSearch = false;
    var appbar = AppBar(
      title: Text(title),
      centerTitle: true,
      leading: IconButton(
        icon: const Icon(Icons.menu),
        onPressed: () => {
          debugPrint('press menu button2S'),
          Scaffold.of(context).openDrawer(),
        },
      ),
      actions: <Widget>[
        Offstage(
            offstage: showSearch,
            child: IconButton(
              icon: const Icon(Icons.search),
              onPressed: () => debugPrint('press search button'),
            ))
      ],
      // bottom: TabBar(
      //   tabs: [
      //     Text("hot"),
      //     Text("home"),
      //     Text("history"),
      //   ],
      // ),
    );

    return appbar;
  }


  PreferredSizeWidget getNormalAppBar(BuildContext context, String title) {
    var appbar = AppBar(
      title: Text(title),
      centerTitle: true,
    );
    return appbar;
  }
}
