import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_test/first/pages/history_screen.dart';
import 'package:flutter_app_test/first/pages/home_screen.dart';
import 'package:flutter_app_test/first/pages/hot_screen.dart';

class FirstBottomBar extends StatelessWidget {

  int _currentItem = 1;

  final List<Widget> _pages = <Widget>[];


  FirstBottomBar({Key? key, required Function(int position) callback})
      :super(key: key) {
    assert(callback != null);
    _pages.add(HotPage());
    _pages.add(HomePage());
    _pages.add(HistoryPage());
  }

  getPageList() {
    return _pages;
  }

  int getItemSize() {
    return _pages.length;
  }

  @override
  Widget build(BuildContext context) {
    var bottomBar = BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.local_fire_department),
          label: '热门',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: '首页',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.history),
          label: '历史',
        ),
      ],
      showSelectedLabels: true,
      selectedItemColor: Colors.red,
      selectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
      selectedIconTheme: const IconThemeData(color: Colors.red),
      currentIndex: _currentItem,
      onTap: null,
      type: BottomNavigationBarType.fixed,
    );

    return bottomBar;
  }

  void setCurrentPage(int position) {
    _currentItem = position;
  }

  getCurrentPosition() {
    return _currentItem;
  }
}
