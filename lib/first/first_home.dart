import 'package:flutter/material.dart';
import 'package:flutter_app_test/first/pages/history_screen.dart';
import 'package:flutter_app_test/first/route/message_home.dart';
import 'package:flutter_app_test/first/route/person_home.dart';
import 'package:flutter_app_test/first/route/setting_home.dart';

import 'app_widgets/first_drawer.dart';
import 'pages/home_screen.dart';
import 'pages/hot_screen.dart';

void main() => runApp(MyFirstApp());

class MyFirstApp extends StatelessWidget {

  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, WidgetBuilder> routeNames = {
      'message':(context) => const MessageHome(),
      'setting':(context) => const SettingHome(),
      'person':(context) => const PersonHome(),
    };

    var materialApp = MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: routeNames,
      title: 'first test',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const MyFirst(),
    );

    return materialApp;
  }
}

class MyFirst extends StatefulWidget {
  const MyFirst({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyFirstMainPage();
  }
}

class MyFirstMainPage extends State<MyFirst> with FirstDrawer {
  int _currentItem = 1;

  final List<Widget> _pages = <Widget>[];


  _changeBottomPosition(int position) {
    setState(() {
      _currentItem = position;
    });
  }

  @override
  void initState() {
    _pages.add(HotPage());
    _pages.add(HomePage());
    _pages.add(HistoryPage());
  }

  @override
  Widget build(BuildContext context) {
    var bottomBar = BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.local_fire_department),
          label: '热门',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: '首页',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.history),
          label: '历史',
        ),
      ],
      showSelectedLabels: true,
      selectedItemColor: Colors.red,
      selectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
      selectedIconTheme: const IconThemeData(color: Colors.red),
      currentIndex: _currentItem,
      onTap: _changeBottomPosition,
      type: BottomNavigationBarType.fixed,
    );

    var scaffold = Scaffold(
      drawer: getDrawer(context),
      bottomNavigationBar: bottomBar,
      body: _pages[_currentItem],
      // bottomNavigationBar: ,
    );

    var defaultTabController = DefaultTabController(
      length: 3,
      child: scaffold,
    );

    return defaultTabController;
  }
}

